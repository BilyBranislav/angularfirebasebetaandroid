package com.pixelart.angularfirebasebeta

import android.app.Application
import com.pixelart.angularfirebasebeta.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class AngularFirebaseBeta : Application() {
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin{
            androidLogger()
            androidContext(this@AngularFirebaseBeta)
            modules(appModule)
        }
    }
}