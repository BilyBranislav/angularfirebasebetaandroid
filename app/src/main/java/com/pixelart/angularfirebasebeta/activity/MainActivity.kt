package com.pixelart.angularfirebasebeta.activity

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.FirebaseApp
import com.pixelart.angularfirebasebeta.R
import com.pixelart.angularfirebasebeta.adapters.NotesAdapter
import com.pixelart.angularfirebasebeta.domain.usecase.FirestoreAllNotesQuery
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import java.time.Duration


class MainActivity : AppCompatActivity(), NotesAdapter.OnNoteClickListener {

    private val TAG = this.javaClass.toString()

    private val firestoreAllNotesQuery: FirestoreAllNotesQuery by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        FirebaseApp.initializeApp(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        val recyclerView = recyclerViewNote
        recyclerView.layoutManager = LinearLayoutManager(this)
        val notesAdapter = NotesAdapter(firestoreAllNotesQuery.getAllNotesQuery(), this)
        recyclerView.adapter = notesAdapter
        notesAdapter.startListening()
    }

    override fun onNoteClick(id: String) {
        Toast.makeText(this, id, Toast.LENGTH_SHORT).show()
    }
}