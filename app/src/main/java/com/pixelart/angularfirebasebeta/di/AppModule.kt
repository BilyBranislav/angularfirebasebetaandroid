package com.pixelart.angularfirebasebeta.di

import com.google.firebase.firestore.FirebaseFirestore
import com.pixelart.angularfirebasebeta.domain.usecase.FirestoreAllNotesQuery
import com.pixelart.angularfirebasebeta.domain.usecase.FireStoreAllNotesQueryImpl
import com.pixelart.angularfirebasebeta.domain.usecase.GetNoteFromDocument
import com.pixelart.angularfirebasebeta.domain.usecase.GetNoteFromDocumentImpl
import org.koin.dsl.module

val appModule = module {

    //Firestore UseCases
    single { FireStoreAllNotesQueryImpl(FirebaseFirestore.getInstance()) as FirestoreAllNotesQuery }
    single { GetNoteFromDocumentImpl() as GetNoteFromDocument }


}