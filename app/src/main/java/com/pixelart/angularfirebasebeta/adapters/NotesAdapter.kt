package com.pixelart.angularfirebasebeta.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import com.pixelart.angularfirebasebeta.R
import com.pixelart.angularfirebasebeta.domain.usecase.GetNoteFromDocument
import kotlinx.android.synthetic.main.recycler_note_item.view.*
import org.koin.core.KoinComponent
import org.koin.core.inject

class NotesAdapter(query: Query, private val onNoteClickListener: OnNoteClickListener): FirestoreAdapter<NotesAdapter.NoteHolder>(query) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_note_item, parent, false)
        return NoteHolder(view, onNoteClickListener)
    }

    override fun onBindViewHolder(holder: NoteHolder, position: Int) {
        holder.setData(getSnapshot(position))
    }


    class NoteHolder(itemView: View, private val onNoteClickListener: OnNoteClickListener): RecyclerView.ViewHolder(itemView),
        KoinComponent {

        private val getNoteFromDocument: GetNoteFromDocument by inject()

        fun setData(documentSnapshot: DocumentSnapshot) {
            val note = getNoteFromDocument.getNote(documentSnapshot)
            itemView.textViewNoteBody.text = note.body
            itemView.setOnClickListener { onNoteClickListener.onNoteClick(documentSnapshot.id) }
        }
    }

    interface OnNoteClickListener {
        fun onNoteClick(id: String)
    }

}