package com.pixelart.angularfirebasebeta.domain.usecase

import com.google.firebase.firestore.DocumentSnapshot
import com.pixelart.angularfirebasebeta.Note

interface GetNoteFromDocument {
    fun getNote(document: DocumentSnapshot): Note
}

class GetNoteFromDocumentImpl: GetNoteFromDocument {
    override fun getNote(document: DocumentSnapshot): Note {
        val note = Note()
        note.body = document.getString("note")
        return note
    }
}