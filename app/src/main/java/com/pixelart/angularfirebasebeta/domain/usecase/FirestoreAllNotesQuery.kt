package com.pixelart.angularfirebasebeta.domain.usecase

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

interface FirestoreAllNotesQuery {
    fun getAllNotesQuery(): Query
}

class FireStoreAllNotesQueryImpl(private val firebaseInstance: FirebaseFirestore) : FirestoreAllNotesQuery {


    override fun getAllNotesQuery(): Query {
        return firebaseInstance.collection("users")
    }
}