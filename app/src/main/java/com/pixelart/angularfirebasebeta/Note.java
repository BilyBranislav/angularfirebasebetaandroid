package com.pixelart.angularfirebasebeta;

public class Note {

    private String body;

    public Note(){}

    public Note(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
